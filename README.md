# My GrimoireLab panels

Custom GrimoireLab panels developed by for demos, talks, or just showcasing some GrimoireLab / Bitergia Analytics features

# How they have been produced

These panels have been done from *standard* GrimoireLab or Bitergia Analytics deployments.

To build your own panel, you can follow [standard Kibana documentation](https://www.elastic.co/guide/en/kibana/6.1/dashboard.html).

To export them into a `.json` file, I use [GrimoireLab Kidash](https://github.com/chaoss/grimoirelab-kidash), with the following command:

```
$ kidash -e <ELASTICSEARCH_ENDPOINT_URL> --dashboard <DASHBOARD_ID> --export <FILENAME>.json
```

Where:

* `<ELASTICSEARCH_ENDPOINT_URL>`: URL to ElasticSearch database (**not to the dashboard**). If you've installed GrimoireLab locally in your computer, it would
something like: `https://localhost:9200`. For Bitergia Analytics deployments is usually something like `https://<project>.biterg.io/data`. 
For example, in [CHAOSS Bitergia Analytics dashboard](https://chaoss.biterg.io), this parameter is 
`https://USER:PASSWORD@chaoss.biterg.io/data` (Yes, you need a `USER:PASSWORD` to access)
* `<DASHBOARD_ID>`: Identifier given by you or by Kibana or Kibiter to the dashboard. To get a list of existing dashboards in a
GrimoireLab / Bitergia Analytics instance, you can use `$ kidash  -e <ELASTICSEARCH_ENDPOINT_URL> --list`
* `<FILENAME>`: Name you want to give to the `.json` file exported

# How to add any of them to your GrimoireLab / Bitergia Analytics instance

Again, using Kidash:

```
$ kidash -e <ELASTICSEARCH_ENDPOINT_URL> --import <FILENAME>.json
```

Where:

* `<ELASTICSEARCH_ENDPOINT_URL>`: URL to ElasticSearch database. If you've installed GrimoireLab locally in your computer, it would
something like: `https://localhost:9200`. For Bitergia Analytics deployments is usually something like `https://<project>.biterg.io/data`. 
For example, in [CHAOSS Bitergia Analytics dashboard](https://chaoss.biterg.io), this parameter is 
`https://USER:PASSWORD@chaoss.biterg.io/data` (Yes, you need a `USER:PASSWORD` to access)
* `<FILENAME>`: Name of the `.json` file to import as a new panel


# Disclaimer

Panels are mostly based in *standard* GrimoireLab / Bitergia Analytics deployments. They should not be using non-standard
parameters or visualizations. But, somethings *shit happens*. Feel free to submit an issue if something doesn't work.

# License

[GPL v3](LICENSE)