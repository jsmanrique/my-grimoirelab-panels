# List of panels

File|Description|Screenshot
----|-----------|----------
[Jono Bacon Consulting panel](C_jbc.json)|Panel with basic Git and GitHub Issues and Pull Requests metrics, requested by Jono Bacon during a conversation in Vancouver during OSSummit'18|![jbc screenshot](screenshots/c_jbc.jpg)